const User = require('../models/user')
const {OAuth2Client} = require('google-auth-library') 
const bcrypt = require('bcryptjs')
const auth = require('../auth')

//Google Login

const clientId = '802333644669-lmmd63hiceb0a0sfrc900e7ondhv56f2.apps.googleusercontent.com'

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),				// para nakarandom yung password eto yung giangawa ni bcypt
		loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})

}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { 
			return { error: 'does-not-exist'} 
		}
		if (user.loginType !== 'email'){							// pang check kung sa gmail siya or sa register 
			return { error: 'login-type-error'}
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return { error: 'incorrect-password'}
		}
	})
}


// Details
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}


//Google Login 

module.exports.verifyGoogleTokenId = async(tokenId) => {	
	console.log(tokenId)
	//middleman
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})
	console.log(data.payload.email_verified)
	
	if(data.payload.email_verified === true){

		const user = await User.findOne({ email: data.payload.email }) 		// chinecheck kung may kaparehas na email sa database 

		if (user !== null ){
			
			if(user.loginType === "google"){
				return{ accessToken: auth.createAccessToken(user.toObject()) }
			} else {
				return {error : "login-type-error"}
			}

		} else {
			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})
		
			return user.save().then((user, err) => {
				return { accessToken : auth.createAccessToken(user.toObject)}
			})

		}
	} else {

		return { error: "google-auth-error" }

	}
	
	
};

module.exports.createCategory = (params) => {
    console.log(params)
    return User.findById(params.userId).then((user) =>{
        user.categories.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType
        });

        return user.save().then((user, err) =>{
            return err ? false : true;
        });
    });
};


module.exports.addRecord = (params) => {
    return User.findById(params.userId).then((user) => {
        user.record.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType,
            amount: params.amount,
            description: params.description
        });
        return user.save().then((user, err) => {
            return (err) ? false : true 
        });
    });
}